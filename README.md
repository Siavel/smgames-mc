# SMGames MC Issues

Issue tracker for SMGames private MC server.
If you run into any problems, go to [the issue tracker](https://bitbucket.org/Siavel/smgames-mc-issues/issues) and see if the issue already exists (feel free to comment if you're having the same problem, whether its closed or not, as long as it is indeed a duplicate issue), or post a new issue if the issue isn't yet known. Be sure to copy your logs to [pastebin](http://pastebin.com) and post the link rather than the full log in your issue.

Always make a note of where you were, and what you were interacting with (or what near, if thats suspicious to you) in the body of your posted issue.

# How do I show you the error??

Open TechnicLauncher, go to [Modpack Options] in the upper right of the window. On the smaller window that comes up, click on [Open] to open the local folder.
*There are two different folders of interest for posting bugs.*

First, open the "**crash-reports**" folder and find the most recent crash log (right click the background of the folder, View -> details, sort by date modified). Open this (notepad's fine, but Notepad++ is ideal) and copy-paste the contents into [pastebin](http://pastebin.com). Second, open "**logs**" folder and do the same thing for "**latest.log**" Open this and copy-paste the contents into [pastebin](http://pastebin.com). 

***Include both when reporting an issue, please.*** It makes it *way easier* for Starr to debug and fix things.

# I need to include an image!
Graphical glitches please take a screenshot ([print screen] button) and add it to your issue.